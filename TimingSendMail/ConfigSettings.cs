﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimingSendMail
{
    public class ConfigSettings
    {
        /// <summary>
        /// 发送邮箱
        /// </summary>
        public string fromMail { get; set; }

        /// <summary>
        /// smtp地址
        /// </summary>
        public string smtp { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public string port { get; set; }

        /// <summary>
        /// 邮箱登录用户名
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// 邮箱登录密码
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// 接收邮箱
        /// </summary>
        public string toMail { get; set; }

        /// <summary>
        /// 抄送地址
        /// </summary>
        public string cc { get; set; }

        /// <summary>
        /// 邮件主题
        /// </summary>
        public string subject { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        public string attach { get; set; }

        /// <summary>
        /// 邮件内容
        /// </summary>
        public string Boby { get; set; }

        /// <summary>
        /// 是否定时发送
        /// </summary>
        public string istime { get; set; }

        /// <summary>
        /// 计划
        /// </summary>
        public string planday { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public string time { get; set; }
    }
}