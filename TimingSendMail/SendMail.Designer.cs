﻿
namespace TimingSendMail
{
    partial class SendMail
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendMail));
            this.textBox_fromMail = new System.Windows.Forms.TextBox();
            this.textBox_toMail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.To = new System.Windows.Forms.Label();
            this.subject = new System.Windows.Forms.Label();
            this.textBox_subject = new System.Windows.Forms.TextBox();
            this.richTextBox_Boby = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_smtp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_port = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_username = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox_Send = new System.Windows.Forms.GroupBox();
            this.groupBox_To = new System.Windows.Forms.GroupBox();
            this.richTextBox_cc = new System.Windows.Forms.RichTextBox();
            this.groupBox_Mailboby = new System.Windows.Forms.GroupBox();
            this.button_Send = new System.Windows.Forms.Button();
            this.richTextBox_attach = new System.Windows.Forms.RichTextBox();
            this.textBox_time = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_plan = new System.Windows.Forms.ComboBox();
            this.checkBox_timer = new System.Windows.Forms.CheckBox();
            this.buttonTime = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.隐藏ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.显示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.groupBoxVerification = new System.Windows.Forms.GroupBox();
            this.buttonCance = new System.Windows.Forms.Button();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_syspassword = new System.Windows.Forms.TextBox();
            this.groupBox_Send.SuspendLayout();
            this.groupBox_To.SuspendLayout();
            this.groupBox_Mailboby.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBoxVerification.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_fromMail
            // 
            this.textBox_fromMail.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_fromMail.Location = new System.Drawing.Point(134, 64);
            this.textBox_fromMail.Name = "textBox_fromMail";
            this.textBox_fromMail.Size = new System.Drawing.Size(260, 25);
            this.textBox_fromMail.TabIndex = 0;
            // 
            // textBox_toMail
            // 
            this.textBox_toMail.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_toMail.Location = new System.Drawing.Point(134, 49);
            this.textBox_toMail.Name = "textBox_toMail";
            this.textBox_toMail.Size = new System.Drawing.Size(260, 25);
            this.textBox_toMail.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "发送邮箱";
            // 
            // To
            // 
            this.To.AutoSize = true;
            this.To.Location = new System.Drawing.Point(23, 378);
            this.To.Name = "To";
            this.To.Size = new System.Drawing.Size(67, 15);
            this.To.TabIndex = 3;
            this.To.Text = "接收邮箱";
            // 
            // subject
            // 
            this.subject.AutoSize = true;
            this.subject.Font = new System.Drawing.Font("宋体", 9F);
            this.subject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.subject.Location = new System.Drawing.Point(17, 54);
            this.subject.Name = "subject";
            this.subject.Size = new System.Drawing.Size(67, 15);
            this.subject.TabIndex = 5;
            this.subject.Text = "邮件主题";
            // 
            // textBox_subject
            // 
            this.textBox_subject.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_subject.Location = new System.Drawing.Point(102, 54);
            this.textBox_subject.Name = "textBox_subject";
            this.textBox_subject.Size = new System.Drawing.Size(427, 25);
            this.textBox_subject.TabIndex = 4;
            // 
            // richTextBox_Boby
            // 
            this.richTextBox_Boby.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTextBox_Boby.Location = new System.Drawing.Point(102, 226);
            this.richTextBox_Boby.Name = "richTextBox_Boby";
            this.richTextBox_Boby.Size = new System.Drawing.Size(427, 212);
            this.richTextBox_Boby.TabIndex = 6;
            this.richTextBox_Boby.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(464, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "邮件内容";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "smtp地址";
            // 
            // textBox_smtp
            // 
            this.textBox_smtp.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_smtp.Location = new System.Drawing.Point(134, 103);
            this.textBox_smtp.Name = "textBox_smtp";
            this.textBox_smtp.Size = new System.Drawing.Size(260, 25);
            this.textBox_smtp.TabIndex = 8;
            this.textBox_smtp.Text = "smtp.163.com";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "端口号";
            // 
            // textBox_port
            // 
            this.textBox_port.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_port.Location = new System.Drawing.Point(134, 149);
            this.textBox_port.Name = "textBox_port";
            this.textBox_port.Size = new System.Drawing.Size(260, 25);
            this.textBox_port.TabIndex = 10;
            this.textBox_port.Text = "25";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 15);
            this.label5.TabIndex = 13;
            this.label5.Text = "邮箱登录用户名";
            // 
            // textBox_username
            // 
            this.textBox_username.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_username.Location = new System.Drawing.Point(134, 192);
            this.textBox_username.Name = "textBox_username";
            this.textBox_username.PasswordChar = '*';
            this.textBox_username.Size = new System.Drawing.Size(260, 25);
            this.textBox_username.TabIndex = 12;
            this.textBox_username.Text = "weichatzhouy";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "邮箱登录密码";
            // 
            // textBox_password
            // 
            this.textBox_password.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_password.Location = new System.Drawing.Point(134, 232);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.PasswordChar = '*';
            this.textBox_password.Size = new System.Drawing.Size(260, 25);
            this.textBox_password.TabIndex = 14;
            this.textBox_password.Text = "FEIGUWINKIHNLPZF";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 420);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "抄送地址";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(18, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "附件";
            // 
            // groupBox_Send
            // 
            this.groupBox_Send.Controls.Add(this.textBox_fromMail);
            this.groupBox_Send.Controls.Add(this.textBox_smtp);
            this.groupBox_Send.Controls.Add(this.textBox_port);
            this.groupBox_Send.Controls.Add(this.textBox_username);
            this.groupBox_Send.Controls.Add(this.textBox_password);
            this.groupBox_Send.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox_Send.ForeColor = System.Drawing.Color.Blue;
            this.groupBox_Send.Location = new System.Drawing.Point(12, 12);
            this.groupBox_Send.Name = "groupBox_Send";
            this.groupBox_Send.Size = new System.Drawing.Size(417, 289);
            this.groupBox_Send.TabIndex = 20;
            this.groupBox_Send.TabStop = false;
            this.groupBox_Send.Text = "发送邮箱设置";
            // 
            // groupBox_To
            // 
            this.groupBox_To.Controls.Add(this.richTextBox_cc);
            this.groupBox_To.Controls.Add(this.textBox_toMail);
            this.groupBox_To.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox_To.ForeColor = System.Drawing.Color.Red;
            this.groupBox_To.Location = new System.Drawing.Point(8, 326);
            this.groupBox_To.Name = "groupBox_To";
            this.groupBox_To.Size = new System.Drawing.Size(417, 244);
            this.groupBox_To.TabIndex = 21;
            this.groupBox_To.TabStop = false;
            this.groupBox_To.Text = "接收邮箱设置";
            // 
            // richTextBox_cc
            // 
            this.richTextBox_cc.Font = new System.Drawing.Font("宋体", 9F);
            this.richTextBox_cc.Location = new System.Drawing.Point(134, 94);
            this.richTextBox_cc.Name = "richTextBox_cc";
            this.richTextBox_cc.Size = new System.Drawing.Size(260, 127);
            this.richTextBox_cc.TabIndex = 22;
            this.richTextBox_cc.Text = "";
            // 
            // groupBox_Mailboby
            // 
            this.groupBox_Mailboby.Controls.Add(this.button_Send);
            this.groupBox_Mailboby.Controls.Add(this.label8);
            this.groupBox_Mailboby.Controls.Add(this.richTextBox_attach);
            this.groupBox_Mailboby.Controls.Add(this.textBox_subject);
            this.groupBox_Mailboby.Controls.Add(this.subject);
            this.groupBox_Mailboby.Controls.Add(this.richTextBox_Boby);
            this.groupBox_Mailboby.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox_Mailboby.ForeColor = System.Drawing.Color.Green;
            this.groupBox_Mailboby.Location = new System.Drawing.Point(447, 22);
            this.groupBox_Mailboby.Name = "groupBox_Mailboby";
            this.groupBox_Mailboby.Size = new System.Drawing.Size(554, 504);
            this.groupBox_Mailboby.TabIndex = 23;
            this.groupBox_Mailboby.TabStop = false;
            this.groupBox_Mailboby.Text = "邮箱内容编辑";
            // 
            // button_Send
            // 
            this.button_Send.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_Send.ForeColor = System.Drawing.Color.DarkGreen;
            this.button_Send.Location = new System.Drawing.Point(102, 454);
            this.button_Send.Name = "button_Send";
            this.button_Send.Size = new System.Drawing.Size(427, 37);
            this.button_Send.TabIndex = 23;
            this.button_Send.Text = "发送";
            this.button_Send.UseVisualStyleBackColor = true;
            this.button_Send.Click += new System.EventHandler(this.button_Send_Click);
            // 
            // richTextBox_attach
            // 
            this.richTextBox_attach.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTextBox_attach.Location = new System.Drawing.Point(102, 99);
            this.richTextBox_attach.Name = "richTextBox_attach";
            this.richTextBox_attach.Size = new System.Drawing.Size(427, 94);
            this.richTextBox_attach.TabIndex = 22;
            this.richTextBox_attach.Text = "";
            // 
            // textBox_time
            // 
            this.textBox_time.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox_time.Location = new System.Drawing.Point(767, 541);
            this.textBox_time.Name = "textBox_time";
            this.textBox_time.Size = new System.Drawing.Size(38, 25);
            this.textBox_time.TabIndex = 24;
            this.textBox_time.Text = "18";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(647, 546);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 15);
            this.label9.TabIndex = 24;
            this.label9.Text = "计划";
            // 
            // comboBox_plan
            // 
            this.comboBox_plan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_plan.Font = new System.Drawing.Font("宋体", 9F);
            this.comboBox_plan.FormattingEnabled = true;
            this.comboBox_plan.Items.AddRange(new object[] {
            "每天",
            "每周"});
            this.comboBox_plan.Location = new System.Drawing.Point(690, 542);
            this.comboBox_plan.Name = "comboBox_plan";
            this.comboBox_plan.Size = new System.Drawing.Size(71, 23);
            this.comboBox_plan.TabIndex = 24;
            // 
            // checkBox_timer
            // 
            this.checkBox_timer.AutoSize = true;
            this.checkBox_timer.Font = new System.Drawing.Font("宋体", 9F);
            this.checkBox_timer.ForeColor = System.Drawing.Color.Black;
            this.checkBox_timer.Location = new System.Drawing.Point(549, 544);
            this.checkBox_timer.Name = "checkBox_timer";
            this.checkBox_timer.Size = new System.Drawing.Size(89, 19);
            this.checkBox_timer.TabIndex = 24;
            this.checkBox_timer.Text = "定时发送";
            this.checkBox_timer.UseVisualStyleBackColor = true;
            this.checkBox_timer.CheckedChanged += new System.EventHandler(this.checkBox_timer_CheckedChanged);
            // 
            // buttonTime
            // 
            this.buttonTime.Font = new System.Drawing.Font("宋体", 9F);
            this.buttonTime.ForeColor = System.Drawing.Color.Black;
            this.buttonTime.Location = new System.Drawing.Point(875, 541);
            this.buttonTime.Name = "buttonTime";
            this.buttonTime.Size = new System.Drawing.Size(101, 23);
            this.buttonTime.TabIndex = 25;
            this.buttonTime.Text = "开始";
            this.buttonTime.UseVisualStyleBackColor = true;
            this.buttonTime.Click += new System.EventHandler(this.buttonTime_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(810, 546);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 15);
            this.label10.TabIndex = 26;
            this.label10.Text = "点";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.隐藏ToolStripMenuItem,
            this.显示ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(109, 76);
            // 
            // 隐藏ToolStripMenuItem
            // 
            this.隐藏ToolStripMenuItem.Name = "隐藏ToolStripMenuItem";
            this.隐藏ToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.隐藏ToolStripMenuItem.Text = "隐藏";
            this.隐藏ToolStripMenuItem.Click += new System.EventHandler(this.隐藏ToolStripMenuItem_Click);
            // 
            // 显示ToolStripMenuItem
            // 
            this.显示ToolStripMenuItem.Name = "显示ToolStripMenuItem";
            this.显示ToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.显示ToolStripMenuItem.Text = "显示";
            this.显示ToolStripMenuItem.Click += new System.EventHandler(this.显示ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // groupBoxVerification
            // 
            this.groupBoxVerification.Controls.Add(this.buttonCance);
            this.groupBoxVerification.Controls.Add(this.buttonConfirm);
            this.groupBoxVerification.Controls.Add(this.label11);
            this.groupBoxVerification.Controls.Add(this.textBox_syspassword);
            this.groupBoxVerification.Location = new System.Drawing.Point(14, 12);
            this.groupBoxVerification.Name = "groupBoxVerification";
            this.groupBoxVerification.Size = new System.Drawing.Size(981, 553);
            this.groupBoxVerification.TabIndex = 28;
            this.groupBoxVerification.TabStop = false;
            this.groupBoxVerification.Visible = false;
            // 
            // buttonCance
            // 
            this.buttonCance.Location = new System.Drawing.Point(500, 263);
            this.buttonCance.Name = "buttonCance";
            this.buttonCance.Size = new System.Drawing.Size(96, 36);
            this.buttonCance.TabIndex = 7;
            this.buttonCance.Text = "退出程序";
            this.buttonCance.UseVisualStyleBackColor = true;
            this.buttonCance.Click += new System.EventHandler(this.buttonCance_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Location = new System.Drawing.Point(347, 263);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(96, 36);
            this.buttonConfirm.TabIndex = 6;
            this.buttonConfirm.Text = "确定";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(336, 213);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 15);
            this.label11.TabIndex = 5;
            this.label11.Text = "密码";
            // 
            // textBox_syspassword
            // 
            this.textBox_syspassword.Location = new System.Drawing.Point(398, 210);
            this.textBox_syspassword.Name = "textBox_syspassword";
            this.textBox_syspassword.PasswordChar = '*';
            this.textBox_syspassword.Size = new System.Drawing.Size(193, 25);
            this.textBox_syspassword.TabIndex = 4;
            this.textBox_syspassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_syspassword_KeyDown);
            // 
            // SendMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1018, 582);
            this.Controls.Add(this.groupBoxVerification);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.buttonTime);
            this.Controls.Add(this.textBox_time);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox_plan);
            this.Controls.Add(this.checkBox_timer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.To);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox_To);
            this.Controls.Add(this.groupBox_Mailboby);
            this.Controls.Add(this.groupBox_Send);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SendMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SendMail_FormClosed);
            this.Load += new System.EventHandler(this.SendMail_Load);
            this.Shown += new System.EventHandler(this.SendMail_Shown);
            this.SizeChanged += new System.EventHandler(this.SendMail_SizeChanged);
            this.groupBox_Send.ResumeLayout(false);
            this.groupBox_Send.PerformLayout();
            this.groupBox_To.ResumeLayout(false);
            this.groupBox_To.PerformLayout();
            this.groupBox_Mailboby.ResumeLayout(false);
            this.groupBox_Mailboby.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBoxVerification.ResumeLayout(false);
            this.groupBoxVerification.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_fromMail;
        private System.Windows.Forms.TextBox textBox_toMail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label To;
        private System.Windows.Forms.Label subject;
        private System.Windows.Forms.TextBox textBox_subject;
        private System.Windows.Forms.RichTextBox richTextBox_Boby;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_smtp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_port;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_username;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox_Send;
        private System.Windows.Forms.GroupBox groupBox_To;
        private System.Windows.Forms.RichTextBox richTextBox_cc;
        private System.Windows.Forms.GroupBox groupBox_Mailboby;
        private System.Windows.Forms.Button button_Send;
        private System.Windows.Forms.RichTextBox richTextBox_attach;
        private System.Windows.Forms.TextBox textBox_time;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_plan;
        private System.Windows.Forms.CheckBox checkBox_timer;
        private System.Windows.Forms.Button buttonTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 隐藏ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 显示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.GroupBox groupBoxVerification;
        private System.Windows.Forms.Button buttonCance;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_syspassword;
    }
}

