﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TimingSendMail
{
    public partial class SendMail : Form
    {
        private System.Threading.Timer timer;
        private ConfigSettings comSettings;

        public SendMail()
        {
            InitializeComponent();
        }

        private void button_Send_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                SetConfig();
                Tools.SendMail(this.comSettings);
            }
        }

        private void checkBox_timer_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_timer.Checked)
            {
                comboBox_plan.Enabled = true;
                textBox_time.Enabled = true;
                buttonTime.Enabled = true;
                groupBox_Mailboby.Enabled = false;
                groupBox_To.Enabled = false;
                groupBox_Send.Enabled = false;
            }
            else
            {
                if (timer != null)
                {
                    timer.Change(-1, 1);
                }
                comboBox_plan.Enabled = false;
                textBox_time.Enabled = false;
                buttonTime.Enabled = false;
                groupBox_Mailboby.Enabled = true;
                groupBox_To.Enabled = true;
                groupBox_Send.Enabled = true;
            }
        }

        private void SendMail_Load(object sender, EventArgs e)
        {
            comboBox_plan.SelectedIndex = 0;
            comboBox_plan.Enabled = false;
            textBox_time.Enabled = false;
            buttonTime.Enabled = false;
            GetConfig();
            if (checkBox_timer.Checked)
            {
                buttonTime_Click(this, null);
            }
            groupBoxVerification.Visible = true;
        }

        private void buttonTime_Click(object sender, EventArgs e)
        {
            switch (comboBox_plan.SelectedIndex)
            {
                case 0://每天
                    this.comSettings.planday = "1";
                    break;

                case 1://每周
                    this.comSettings.planday = "7";
                    break;

                default:
                    this.comSettings.planday = "1";
                    break;
            }
            this.comSettings.time = textBox_time.Text.Trim();
            if (IsValid())
            {
                buttonTime.Enabled = false;
                comboBox_plan.Enabled = false;
                textBox_time.Enabled = false;
                SetConfig();
                setTaskAtFixedTime();
            }
        }

        /// <summary>
        /// 获取配置文件参数
        /// </summary>
        private void GetConfig()
        {
            this.comSettings = Tools.GetComSettings();
            textBox_fromMail.Text = this.comSettings.fromMail;
            textBox_smtp.Text = this.comSettings.smtp;
            textBox_port.Text = this.comSettings.port;
            textBox_username.Text = this.comSettings.username;
            textBox_password.Text = this.comSettings.password;
            textBox_toMail.Text = this.comSettings.toMail;
            richTextBox_cc.Text = this.comSettings.cc;
            textBox_subject.Text = this.comSettings.subject;
            richTextBox_attach.Text = this.comSettings.attach;
            richTextBox_Boby.Text = this.comSettings.Boby;
            if (this.comSettings.istime == "1")
            {
                checkBox_timer.Checked = true;
            }
            else
            {
                checkBox_timer.Checked = false;
            }
            comboBox_plan.SelectedIndex = Convert.ToInt32(this.comSettings.planday);
            textBox_time.Text = this.comSettings.time;
        }

        /// <summary>
        /// 获取配置文件参数
        /// </summary>
        private void SetConfig()
        {
            this.comSettings.fromMail = textBox_fromMail.Text;
            this.comSettings.smtp = textBox_smtp.Text;
            this.comSettings.fromMail = textBox_fromMail.Text;
            this.comSettings.port = textBox_port.Text;
            this.comSettings.username = textBox_username.Text;
            this.comSettings.password = textBox_password.Text;
            this.comSettings.toMail = textBox_toMail.Text;
            this.comSettings.cc = richTextBox_cc.Text;
            this.comSettings.subject = textBox_subject.Text;
            this.comSettings.attach = richTextBox_attach.Text;
            this.comSettings.Boby = richTextBox_Boby.Text;

            if (checkBox_timer.Checked)
            {
                this.comSettings.istime = "1";
            }
            else
            {
                this.comSettings.istime = "0";
            }
            this.comSettings.planday = comboBox_plan.SelectedIndex.ToString();
            this.comSettings.time = textBox_time.Text;
            Tools.SetConfig(this.comSettings);
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <returns></returns>
        private bool IsValid()
        {
            if (string.IsNullOrEmpty(textBox_fromMail.Text))
            {
                MessageBox.Show("请录入发送邮箱");
                return false;
            }
            if (!Tools.IsValidEmail(textBox_fromMail.Text))
            {
                MessageBox.Show("发送邮箱不是有效的邮箱格式");
                return false;
            }
            if (string.IsNullOrEmpty(textBox_smtp.Text))
            {
                MessageBox.Show("请录入邮箱smtp地址");
                return false;
            }
            if (string.IsNullOrEmpty(textBox_port.Text))
            {
                MessageBox.Show("请录入端口号");
                return false;
            }
            if (string.IsNullOrEmpty(textBox_username.Text))
            {
                MessageBox.Show("请录入邮箱登录用户名");
                return false;
            }
            if (string.IsNullOrEmpty(textBox_password.Text))
            {
                MessageBox.Show("请录入邮箱登录密码");
                return false;
            }
            if (!Tools.IsValidEmail(textBox_toMail.Text))
            {
                MessageBox.Show("接收邮箱不是有效的邮箱格式");
                return false;
            }
            if (string.IsNullOrEmpty(textBox_toMail.Text))
            {
                MessageBox.Show("请录入接收邮箱");
                return false;
            }
            if (string.IsNullOrEmpty(textBox_subject.Text))
            {
                MessageBox.Show("请录入主题");
                return false;
            }
            if (!string.IsNullOrEmpty(richTextBox_cc.Text))
            {
                if (!Tools.IsValidEmail(richTextBox_cc.Text))
                {
                    MessageBox.Show("抄送邮箱不是有效的邮箱格式");
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(richTextBox_attach.Text))
            {
                if (!File.Exists(richTextBox_attach.Text))
                {
                    MessageBox.Show("附件文件不存在");
                    return false;
                }
            }
            if (checkBox_timer.Checked)
            {
                if (string.IsNullOrEmpty(textBox_time.Text))
                {
                    MessageBox.Show("请录入时间");
                    return false;
                }
                double dou_time = 0;
                if (!double.TryParse(textBox_time.Text, out dou_time))
                {
                    MessageBox.Show("请录入有效的数字");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 设置定时器在零点执行任务
        /// </summary>
        /// <param name="day">计划执行天数</param>
        private void setTaskAtFixedTime()
        {
            DateTime now = DateTime.Now;
            DateTime zeroOClock = DateTime.Today.AddHours(Convert.ToDouble(this.comSettings.time)); //凌晨00:00:00
            if (now > zeroOClock)
            {
                zeroOClock = zeroOClock.AddDays(Convert.ToDouble(this.comSettings.planday));
            }
            int msUntilFour = (int)((zeroOClock - now).TotalMilliseconds);

            timer = new System.Threading.Timer(doAt0AM);
            timer.Change(msUntilFour, Timeout.Infinite);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="state"></param>
        private void doAt0AM(object state)
        {
            try
            {
                //调用数据备份功能
                Tools.SendMail(this.comSettings);
                setTaskAtFixedTime();
            }
            catch (Exception se)
            {
                Tools.WriteTxt("发送失败:" + se.Message);
            }
            finally
            {
                //再次设定
                setTaskAtFixedTime();
            }
        }

        private void 隐藏ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            base.Hide();
        }

        private void 显示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBoxVerification.Visible = true;
            base.Show();
            base.WindowState = FormWindowState.Normal;
            base.Activate();
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBoxVerification.Visible = true;
            base.Show();
            base.WindowState = FormWindowState.Normal;
            base.Activate();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (base.WindowState == FormWindowState.Minimized)
            {
                base.WindowState = FormWindowState.Normal;
                this.notifyIcon1.Visible = false;
                base.ShowInTaskbar = true;
                this.Show();
            }
        }

        private void SendMail_SizeChanged(object sender, EventArgs e)
        {
            if (base.WindowState == FormWindowState.Minimized)
            {
                base.ShowInTaskbar = false;
                this.notifyIcon1.Visible = true;
                this.notifyIcon1.Text = "日志监控V1.0";
                this.Hide();
            }
        }

        private void SendMail_Shown(object sender, EventArgs e)
        {
            if (this.comSettings.istime == "1")
            {
                base.WindowState = FormWindowState.Minimized;
                base.Hide();
            }
            else
            {
                base.WindowState = FormWindowState.Normal;
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (textBox_syspassword.Text == "system")
            {
                groupBoxVerification.Visible = false;
                textBox_syspassword.Text = "";
            }
            else
            {
                MessageBox.Show("管理员密码不正确", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox_syspassword.Focus();
                return;
            }
        }

        private void buttonCance_Click(object sender, EventArgs e)
        {
            if (textBox_syspassword.Text == "system")
            {
                this.notifyIcon1.Visible = false;
                base.Close();
                base.Dispose();
                Application.Exit();
            }
            else
            {
                MessageBox.Show("管理员密码不正确", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox_syspassword.Focus();
                return;
            }
        }

        private void SendMail_FormClosed(object sender, FormClosedEventArgs e)
        {
            groupBoxVerification.Visible = true;
            if (textBox_syspassword.Text == "system")
            {
                this.notifyIcon1.Visible = false;
                base.Close();
                base.Dispose();
                Application.Exit();
            }
            else
            {
                MessageBox.Show("管理员密码不正确", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox_syspassword.Focus();
                return;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.ShowInTaskbar = false;
            this.WindowState = FormWindowState.Minimized;
            e.Cancel = true;
        }

        private void textBox_syspassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonConfirm_Click(this, null);
            }
        }
    }
}