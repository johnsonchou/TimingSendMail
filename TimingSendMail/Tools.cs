﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace TimingSendMail
{
    public static class Tools
    {
        public static XDocument xDocument;
        public static string xmlPath = "";

        /// <summary>
        /// 检查配置文件
        /// </summary>
        /// <returns></returns>
        public static bool CheckFile()
        {
            xmlPath = Assembly.GetExecutingAssembly().Location;
            xmlPath = xmlPath.Substring(0, xmlPath.LastIndexOf('\\'));
            xmlPath += "\\LocalConfig.xml";
            if (!System.IO.File.Exists(xmlPath))
            {
                throw new Exception("本地配置文件不存在！");
            }
            xDocument = XDocument.Load(xmlPath);
            return true;
        }

        /// <summary>
        /// 获取串口配置
        /// </summary>
        /// <returns></returns>
        public static ConfigSettings GetComSettings()
        {
            if (CheckFile())
            {
                ConfigSettings model = new ConfigSettings();
                var query = (from ac in xDocument.Descendants("LocalConfig").Elements()
                             select ac).ToList();
                if (query == null)
                {
                    throw new Exception("本地配置文件配置不正确！");
                }
                PropertyInfo[] itemPro = model.GetType().GetProperties();
                foreach (var que in query)
                {
                    foreach (PropertyInfo pi in itemPro)
                    {
                        if (que.Attribute("name").Value == pi.Name)
                        {
                            pi.SetValue(model, que.Attribute("value").Value.ToString(), null);
                            break;
                        }
                    }
                }
                return model;
            }
            return null;
        }

        /// <summary>
        /// 设置参数配置
        /// </summary>
        /// <returns></returns>
        public static void SetConfig(ConfigSettings model)
        {
            try
            {
                if (CheckFile())
                {
                    var query = (from ac in xDocument.Descendants("LocalConfig").Elements()
                                 select ac).ToList();
                    if (query == null)
                    {
                        throw new Exception("本地配置文件配置不正确！");
                    }
                    PropertyInfo[] itemPro = model.GetType().GetProperties();
                    foreach (var item in query)
                    {
                        foreach (PropertyInfo pi in itemPro)
                        {
                            if (item.Attribute("name").Value == pi.Name)
                            {
                                string ls_value = pi.GetValue(model, null).ToString();
                                if (!string.IsNullOrEmpty(ls_value))
                                {
                                    item.Attribute("value").Value = ls_value;
                                }
                                break;
                            }
                        }
                    }
                    xDocument.Save(xmlPath);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void SendMail(ConfigSettings configSettings)
        {
            try
            {
                //定义一个MailMessage对象
                //from email，to email，主题，邮件内容
                MailMessage mailmessage = new MailMessage(configSettings.fromMail, configSettings.toMail, configSettings.subject, configSettings.Boby);
                mailmessage.Priority = MailPriority.Normal; //邮件优先级
                SmtpClient smtpClient = new SmtpClient(configSettings.smtp, Convert.ToInt32(configSettings.port));//smtp地址以及端口号
                smtpClient.Credentials = new NetworkCredential(configSettings.username, configSettings.password);//smtp用户名密码
                smtpClient.EnableSsl = true; //启用ssl
                //抄送
                if (!string.IsNullOrEmpty(configSettings.cc))
                {
                    MailAddress cc = new MailAddress(configSettings.cc);//获取输入的抄送人邮箱地址
                    mailmessage.CC.Add(cc);
                }
                if (!string.IsNullOrEmpty(configSettings.attach))
                {
                    //附件
                    Attachment attach = new Attachment(configSettings.attach);
                    mailmessage.Attachments.Add(attach);
                }
                smtpClient.Send(mailmessage); //发送邮件
            }
            catch (SmtpException se) //smtp错误
            {
                throw se;
            }
        }

        /// <summary>
        /// 写日志
        /// </summary>
        public static void WriteTxt(string txt)
        {
            DateTime dateTime = DateTime.Now;
            string strDate = System.DateTime.Now.ToString("yyMMdd");
            string path = @"\ErrorLog\";
            string strtxt = "" + dateTime.ToString() + "\r\n" + txt;
            if (!Directory.Exists(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + path))
            {
                Directory.CreateDirectory(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + path);
            }
            using (StreamWriter sw = new StreamWriter(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + path + strDate + ".txt", true, Encoding.UTF8))
            {
                sw.Write("----------------------------------------------------------------------------------------------------" + "\r\n");
                sw.Write(strtxt + '\r' + '\n');
                sw.Close();
            }
        }

        /// <summary>
        /// 验证邮箱地址
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}